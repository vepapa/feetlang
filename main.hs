import Data.Map.Strict (Map, fromList, toList, (!))
import Text.Parsec
  ( ParseError,
    char,
    choice,
    digit,
    letter,
    many1,
    option,
    parse,
    spaces,
    string,
    try,
    (<|>),
  )
import Text.Parsec.String (Parser)

data FeetExpr
  = FeetNumber Int
  | FeetOp String FeetExpr FeetExpr

instance Show FeetExpr where
  show (FeetNumber num) = "feet" ++ show num
  show (FeetOp op expr1 expr2) = "feet" ++ " " ++ op ++ " " ++ show expr1 ++ " " ++ show expr2 ++ " " ++ "~"

env :: Map String (Int -> Int -> Int)
env =
  fromList
    [ ("feet!", (+))
    ]

eval :: FeetExpr -> Int
eval (FeetOp op expr1 expr2) = (env ! op) (eval expr1) (eval expr2)
eval (FeetNumber num) = num

parseNumber :: Parser FeetExpr
parseNumber = do
  sign <- option "" (string "-")
  string "feet"
  num <- many1 digit
  return (FeetNumber (read (sign ++ num)))

-- parseBool :: Parser FeetExpr
-- parseBool = do
--   string "feet"
--   bool <- choice [try (string "rue" >> return True), try (string "alse" >> return False)]
--   return (FeetBool bool)

parseBinarySExp :: Parser FeetExpr
parseBinarySExp = do
  string "feet"
  spaces
  op <- choice (map (string . fst) (toList env))
  spaces
  expr1 <- try parseBinarySExp <|> try parseNumber
  spaces
  expr2 <- try parseBinarySExp <|> try parseNumber
  spaces
  char '~'
  return (FeetOp op expr1 expr2)

parseFeet :: Parser FeetExpr
-- parseFeet = try parseBinarySExp <|> try parseNumber <|> try parseBool <|> try parseSymbol
parseFeet = try parseBinarySExp <|> try parseNumber

parseFeetExp :: String -> Either ParseError FeetExpr
parseFeetExp = parse parseFeet ""

main :: IO ()
main = do
  line <- getLine
  case parseFeetExp line of
    Right expr -> print $ eval expr
    Left error -> print error
  main